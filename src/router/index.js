import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import ContactsIndex from '@/components/ContactsIndex'
import ContactsShow from '@/components/ContactsShow'
import ContactsCreate from '@/components/ContactsCreate'
import ContactsUpdate from '@/components/ContactsUpdate'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/contacts',
      name: 'contacts.index',
      component: ContactsIndex
    },
    {
      path: '/contacts/create',
      name: 'contacts.create',
      component: ContactsCreate
    },
    {
      path: '/contacts/:id/update/',
      name: 'contacts.update',
      component: ContactsUpdate
    },
    {
      path: '/contacts/:id',
      name: 'contacts.show',
      component: ContactsShow
    }

  ]
})
